$(document).ready(function(){

  //expand submenu
  $('.expand-menu').click(function() {
    var menu = $('.menu');
    if (menu.hasClass('menu-tiny')) {
      $(this).removeClass('active');
      menu.removeClass('menu-tiny')
        .slideUp();
      $('.navigator').addClass('navigator-menu');
    } else {
      $(this).addClass('active');
      menu.addClass('menu-tiny')
        .slideDown();
      $('.navigator').removeClass('navigator-menu');
    }
  });

  //heartslider
  $('#heartslider').owlCarousel({
    items:1,
    loop:true,
    margin:0,
    responsiveClass:false,
    nav:true,
    dots:true,
    autoplay:true,
    autoHeight:false,
    autoplayTimeout:4000,
    autoplaySpeed:1000,
    autoplayHoverPause:false,
    navText:false,
    onInitialize: function (event) {
      if ($('.owl-carousel .h-item').length <= 1) {
        this.settings.loop = false;
      }
    }
  });

  //heartslider
  $('#slider_desgin').owlCarousel({
    items:1,
    loop:true,
    margin:0,
    responsiveClass:false,
    nav:true,
    dots:true,
    autoplay:true,
    autoHeight:false,
    autoplayTimeout:4000,
    autoplaySpeed:1000,
    autoplayHoverPause:false,
    navText:false,
    onInitialize: function (event) {
      if ($('.owl-carousel .h-item').length <= 1) {
        this.settings.loop = false;
      }
    }
  });

});